import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams.update({'font.size': 12})

mlist = ['waterfowl', 'wellhead_protection', 'pheasant', 'population', 'lake_recreation']

title_dict = {'bird_watching': 'Bird Watching',
              'lake_recreation': 'Lake Recreation',
              'pheasant': 'Pheasant Habitat',
              'population': 'Population Served',
              'sgcn_all': 'SGCN Breeding Habitat Quality',
              'upland_game_birds': 'Upland Game Species Breeding Habitat',
              'waterfowl': 'Waterfowl Breeding Habitat Quality',
              'wellhead_protection': 'Wellhead Protection'}

df = pd.read_csv('c:/users/rrnoe/desktop/test.csv')
df.set_index('metric', inplace=True)

df = df.loc[df['quality'] == 'Best']
df['total'] = df['other'] + df['wma'] + df['ls']
df['pct_other'] = df['other'] / df['total']
df['pct_wma'] = df['wma'] / df['total']
df['pct_ls'] = df['ls'] / df['total']

ls_list = []
wma_list = []
other_list = []

for metric in mlist:
    quality = 'Best'
    ls_val = df.loc[quality + ' ' + metric, 'pct_ls']
    ls_list.append(ls_val)

    wma_val = df.loc[quality + ' ' + metric, 'pct_wma']
    wma_list.append(wma_val)

    other_val = df.loc[quality + ' ' + metric, 'pct_other']
    other_list.append(other_val)


width = 0.3  # the width of the bars: can also be len(x) sequence
ind = np.arange(len(mlist))
bars_1and2 = np.add(ls_list, wma_list).tolist()
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(1, 1, 1)
p2 = plt.bar(ind, ls_list, width, color='#FF7F0E')
p1 = plt.bar(ind, wma_list, width, color='#1F77B4', bottom=ls_list)
# p0 = plt.bar(ind, other_list, width, color='green', bottom=bars_1and2)


plt.ylabel('% of Protected Area in State')
plt.title('Portfolio of Best Quality of Lessard-Sams Acquisitions')
plt.xticks(ind, mlist)
# ax.set_yticklabels(['{:,}'.format(int(x)) for x in ax.get_yticks().tolist()])
# plt.legend((p2[0], p1[0]), ('Lessard-Sams\n acquisitions', 'WMAs without \nL-S acquisitions'))
# ax.legend(('Lessard-Sams\nacquisitions',))
fig.savefig('portfolio_ls.png', bbox_inches='tight')
plt.show()