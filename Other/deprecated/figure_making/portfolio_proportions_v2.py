import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams.update({'font.size': 12})

mlist = ['bird_watching', 'lake_recreation', 'pheasant', 'population',
         'sgcn_all', 'upland_game_birds', 'waterfowl', 'wellhead_protection']

title_dict = {'bird_watching': 'Bird Watching',
              'lake_recreation': 'Lake Recreation',
              'pheasant': 'Pheasant',
              'population': 'Nearby Population',
              'sgcn_all': 'Non-game Birds',
              'upland_game_birds': 'Upland Game Birds',
              'waterfowl': 'Waterfowl',
              'wellhead_protection': 'Wellhead Protection'}

df = pd.read_csv('c:/users/rrnoe/desktop/test30.csv')
df.set_index('metric', inplace=True)

best_state_area = 65290660
ls_area = 1549405
df.rename(columns={'1': 'other', '4': 'wma', '9': 'ls', '12': 'unprotected'}, inplace=True)
# df = df[['other', 'wma', 'ls', 'unprotected']] * 0.2223869

df = df.loc[df['quality'] == 'Best']
df = df[df['name'].isin(mlist)]
df['total'] = df['other'] + df['wma'] + df['ls']

df['best_resource_area'] = df['other'] + df['wma'] + df['ls'] + df['unprotected']
df['ls_portfolio_adj'] = df['best_resource_area'] / best_state_area
df['adj_ls_portfolio_area'] = df['ls_portfolio_adj'] * ls_area
df['result'] = df['ls'] / df['adj_ls_portfolio_area']
df['result2'] = df['ls'] / df['best_resource_area']
df.sort_values('result', ascending=False, inplace=True)
result = df['result'].tolist()
result_names = df['name'].tolist()
pretty_names = [title_dict[x] for x in result_names]

width = 0.3  # the width of the bars: can also be len(x) sequence
ind = np.arange(len(mlist))
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(1, 1, 1)
p2 = plt.bar(ind, result, width, color='#FF7F0E')

plt.ylabel('Proportion of LSOHF Portfolio')
plt.title('Proportion of LSOHF portfolio in Highest Quality Category\n '
          'Relative to Amount of Highest Quality Land in the State')
vals = ax.get_yticks()
ax.set_yticklabels(['{:,.0%}'.format(x) for x in vals])
plt.axhline(y=0.25, color='g', linestyle='-')
plt.xticks(ind, pretty_names, rotation=90)
fig.savefig('portfolio_ls2.png', bbox_inches='tight')
plt.show()