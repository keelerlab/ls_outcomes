import numpy as np
import geopandas as gp
import pandas as pd
from rasterstats import zonal_stats
import matplotlib.pyplot as plt


# Parameters
potential_path = 'viable_sample_scored.csv'
pastacq_path = 'ls_scored.csv'

metric_list = ['mallards', 'trout', 'upland_birds',  'pheasant_hunting', 'risk_dev', 'turkey', 'risk_ag']


print_metrics = {
    'mallards': 'Mallards',
    'upland_birds': 'Grouse and Woodcock',
    'gwn': 'Groundwater Nitrate',
    'trails': 'Recreation Trails',
    'carbon': 'Soil Carbon',
    'turkey': 'Turkey',
    'trout': 'Trout Angling',
    'bird_watching': 'Bird Watching',
    'pheasant_hunting': 'Pheasant',
    'wild_rice': 'Wild Rice',
    'population': 'Nearby Population',
    'risk_dev': 'Risk of Conversion to Developed',
    'risk_ag': 'Risk of Conversion to Ag'}

print_list = []
for metric in metric_list:
    print_list.append(print_metrics[metric])
# End parameters


def calc_index(all_parcels_df, metric_list):
    for metric in metric_list:
        metric = metric+'_mean'
        col_min = all_parcels_df[metric].min()
        col_max = all_parcels_df[metric].max()
        col_range = col_max - col_min
        all_parcels_df['idx_' + metric] = (all_parcels_df[metric] - col_min) / col_range
        pastacq_df = all_parcels_df.loc[all_parcels_df['forty_id'].str.startswith('l')].copy()
        sample_df = all_parcels_df.loc[all_parcels_df['forty_id'].str.startswith('f')].copy()
    return pastacq_df, sample_df

# Read in parcel score and other data
potential = pd.read_csv(potential_path)
pastacq = pd.read_csv(pastacq_path)

# Calculate a new index score relative to the viable parcels
all_parcels = pd.concat([potential, pastacq])
pastacq, potential = calc_index(all_parcels, metric_list)

for metric in metric_list:
    fig = plt.figure(figsize=(6.5, 3.75))
    ax2 = fig.add_subplot(1, 1, 1)
    # viable_avg = potential[metric+'_mean'].median()
    # viable_risk_avg = potential['risk_dev_mean'].median()
    # pastacq_avg = pastacq[metric+'_mean'].median()
    # pastacq_risk_avg = pastacq['risk_dev_mean'].median()
    # ax2.scatter(pastacq[metric+'_mean'], pastacq['risk_dev_mean'], color='#003865', label='Past Acquisitions', s=4).set_zorder(2)
    viable_avg = potential['idx_'+metric+'_mean'].median()
    viable_risk_avg = potential['idx_risk_dev_mean'].median()
    pastacq_avg = pastacq['idx_'+metric+'_mean'].median()
    pastacq_risk_avg = pastacq['idx_risk_dev_mean'].median()
    # ax2.scatter(pastacq['idx_'+metric+'_mean'], pastacq['idx_risk_dev_mean'], color='#003865', label='Past Acquisitions', s=4).set_zorder(2)
    ax2.axvline(x=viable_avg, color='#78BE21', linestyle='--', linewidth=2, label='Random Selection Median').set_zorder(3)
    ax2.axhline(y=viable_risk_avg, color='#78BE21', linestyle='--', linewidth=2).set_zorder(3)
    ax2.axvline(x=pastacq_avg, color='#E57200', linestyle='-', linewidth=2, label='LSOHF parcels Median').set_zorder(3)
    ax2.axhline(y=pastacq_risk_avg, color='#E57200', linestyle='-', linewidth=2).set_zorder(3)
    # ax2.axvline(x=viable_1sd, color='#9ED060', linestyle='-.', linewidth=2, label='Viable Avg + 1 SD').set_zorder(0)
    # ax2.axvline(x=viable_2sd, color='#C5E39F', linestyle=':', linewidth=2, label='Viable Avg + 2 SD').set_zorder(0)
    ax2.set_xlabel(print_metrics[metric]+' Score')
    ax2.set_ylabel('Risk of Development')
    ax2.set_xlim([-0.01, 1.05])
    ax2.set_ylim([-0.01, 1.05])
    ax2.set_title(print_metrics[metric]+' Scores For Past Acquisitions')

    ax2.legend(loc='upper right', framealpha=0.1)

    plt.tight_layout()
    plt.show()
    # plt.savefig('idxmedian_'+metric+'_scatter_with_lines.png', bbox_inches='tight')
    # plt.savefig('median_'+metric+'_scatter_with_lines.png', bbox_inches='tight')


