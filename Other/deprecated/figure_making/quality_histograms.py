import numpy as np
import geopandas as gp
import pandas as pd
import matplotlib.pyplot as plt


# Parameters
ls_path = 'scored/easement_and_fee_scored.shp'
wma_path = 'scored/wma_minus_ls_scored.shp'

# mlist = ['bird_watching.tif', 'lake_recreation.tif', 'pheasant.tif', 'pollination.tif', 'population.tif',
#          'sgcn_all.tif', 'sgcn_forest.tif', 'sgcn_grassland.tif', 'sgcn_wetland.tif', 'species_richness.tif',
#          'trout_angling.tif', 'upland_game_birds.tif', 'waterfowl.tif', 'wellhead_protection.tif']
mlist = ['bird_watching.tif', 'lake_recreation.tif', 'pheasant.tif']
mlist = ['idx/' + x for x in mlist]

# End parameters

# Read in parcel score and other data
ls = gp.read_file(ls_path)
wma = gp.read_file(wma_path)

for metric in mlist:
    fig = plt.figure(figsize=(8, 6))
    ax1 = fig.add_subplot(1, 1, 1)
    metric = metric[:-4]
    # ax1.hist(ls[metric[4:14]], log=True, color='#78BE21', edgecolor='white', linewidth=0.5, stacked=True, label='Other potential parcels')
    ax1.hist([wma['pheasant'], ls['pheasant']], stacked=True, label='label')
    # ax1.hist(x, stacked=True, label='Other potential parcels')
    # ax1.set_xlabel(print_metrics[metric])
    ax1.set_ylabel('y axis')
    # ax1.set_xlim([-0.05, 1.05])
    ax1.set_title('title')
    ax1.legend(loc='upper right')


    plt.tight_layout()

plt.tight_layout()
plt.show()
# plt.savefig('all_hist+scatter.pdf', bbox_inches='tight')


