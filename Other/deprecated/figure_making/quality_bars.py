import numpy as np
import geopandas as gp
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams.update({'font.size': 14})

mlist = ['bird_watching.tif', 'lake_recreation.tif', 'pheasant.tif', 'population.tif',
         'sgcn_all.tif', 'upland_game_birds.tif', 'waterfowl.tif', 'wellhead_protection.tif']

title_dict = {'bird_watch': 'Bird Watching',
              'lake_recre': 'Lake Recreation',
              'pheasant': 'Pheasant Habitat',
              'population': 'Population Served',
              'sgcn_all': 'SGCN Breeding Habitat Quality',
              'upland_gam': 'Upland Game Species Breeding Habitat',
              'waterfowl': 'Waterfowl Breeding Habitat Quality',
              'wellhead_p': 'Wellhead Protection'}

ls = gp.read_file('scored/ls_tax_intersect_scored.shp')
wma = gp.read_file('scored/wma_minus_ls_scored.shp')
ls['gis_acres'] = ls['geometry'].area / 4046.86
wma['gis_acres'] = wma['geometry'].area / 4046.86
tax = gp.read_file('scored/sample10pct_tax_undev_scored.shp')

for metric in mlist:
    metric = metric[:-4]
    metric = metric[:10]
    print(metric)
    if metric in ['wellhead_p', 'lake_recre', 'pheasant']:
        tax_no0 = tax[tax[metric] != 0].copy()
        ls_no0 = ls[ls[metric] != 0].copy()
        wma_no0 = wma[wma[metric] != 0].copy()
        tax_no0['q_' + metric], bins = pd.qcut(tax_no0[metric], 4, retbins=True, labels=False)
        ls_no0['q_' + metric] = pd.cut(ls_no0[metric], bins=bins, labels=False)
        wma_no0['q_' + metric] = pd.cut(wma_no0[metric], bins=bins, labels=False)
        ls_grouped = ls_no0.groupby('q_' + metric).agg({'gis_acres': 'sum'})
        wma_grouped = wma_no0.groupby('q_' + metric).agg({'gis_acres': 'sum'})
        ls_grouped.index = ls_grouped.index.astype(int)
        wma_grouped.index = wma_grouped.index.astype(int)


    else:
        tax['q_' + metric], bins = pd.qcut(tax[metric], 4, retbins=True, labels=False)
        ls['q_' + metric] = pd.cut(ls[metric], bins=bins, labels=False)
        wma['q_' + metric] = pd.cut(wma[metric], bins=bins, labels=False)
        tax_grouped = tax.groupby('q_' + metric).agg({'gis_acres': 'sum'})
        ls_grouped = ls.groupby('q_' + metric).agg({'gis_acres': 'sum'})
        wma_grouped = wma.groupby('q_' + metric).agg({'gis_acres': 'sum'})
        tax_grouped.index = tax_grouped.index.astype(int)
        ls_grouped.index = ls_grouped.index.astype(int)
        wma_grouped.index = wma_grouped.index.astype(int)


    ls_list = []
    wma_list = []
    qlist = [0, 1, 2, 3]
    for q_pos in qlist:
        if q_pos in ls_grouped.index:
            val = ls_grouped.at[q_pos, 'gis_acres']
        else:
            val = 0
        ls_list.append(val)
    for q_pos in qlist:
        if q_pos in wma_grouped.index:
            val = wma_grouped.at[q_pos, 'gis_acres']
        else:
            val = 0
        wma_list.append(val)

    width = 0.35  # the width of the bars: can also be len(x) sequence
    ls_list.pop(0)
    wma_list.pop(0)
    ind = np.arange(len(ls_list))

    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(1, 1, 1)
    # p1 = plt.bar(ind, wma_list, width, color='#1F77B4')
    p1 = plt.bar(ind, wma_list, width, color='white', bottom=ls_list)
    # p2 = plt.bar(ind, ls_list, width, color='#FF7F0E', bottom=wma_list)
    p2 = plt.bar(ind, ls_list, width, color='#FF7F0E')
    # p2 = plt.bar(ind, ls_list, width, color='#FF7F0E')
    plt.ylabel('Number of Acres')
    plt.title(title_dict[metric] + '\nQuality of Lessard-Sams Acquisitions')
    plt.xticks(ind, ('Moderate\n25-50th %tile', 'High\n50-75th %tile', 'Very High\n >75th %tile'))
    ax.set_yticklabels(['{:,}'.format(int(x)) for x in ax.get_yticks().tolist()])
    # plt.legend((p2[0], p1[0]), ('Lessard-Sams\n acquisitions', 'WMAs without \nL-S acquisitions'))
    ax.legend(('Lessard-Sams\nacquisitions',))
    fig.savefig(metric+'_ls.png', bbox_inches='tight')
    plt.show()