import pandas as pd
import geopandas as gp

# Path to the data supplied by LSOHC. Assumed to contain all known parcel boundaries, protect type, and their customid
parcels_path = 'data/ParcelShapefile.shp'

# Read in provided data
parcels = gp.read_file(parcels_path)

# Sometimes their customid improperly includes quotes, this strips them out if applicable
parcels['cid'] = parcels['customid'].str.replace('"', '')

# Boundary data is primarily for fee and easement acquisitions, but is sometimes available for enhancement or 
# restoration. Those types are less well defined so are being excluded for now.
# test edit
keep_project_types = ['Protect in Fee with PILT', 'Protect in Fee w/o PILT', 'Protect in Easement']
parcels = parcels.loc[parcels['project_ty'].isin(keep_project_types)]

