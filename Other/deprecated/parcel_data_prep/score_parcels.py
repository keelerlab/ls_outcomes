import geopandas as gp
import pandas as pd
from rasterstats import zonal_stats

# Step

def mean_parcel_score(parcel_df, metric_paths_dict):

    for metric in metric_paths_dict:
        metric_name = metric[4:-4]
        print(metric_name)
        score_mean = zonal_stats(parcel_df, metric, stats=['mean'], all_touched=False)
        score_mean = pd.DataFrame(score_mean)
        parcel_df = parcel_df.join(score_mean)
        parcel_df.rename(columns={'mean': metric_name}, inplace=True)
        parcel_df[metric_name].fillna(0, inplace=True)

    return parcel_df


# mlist = ['bird_watching.tif', 'lake_recreation.tif', 'pheasant.tif', 'pollination.tif', 'population.tif',
#          'sgcn_all.tif', 'sgcn_forest.tif', 'sgcn_grassland.tif', 'sgcn_wetland.tif', 'species_richness.tif',
#          'trout_angling.tif', 'upland_game_birds.tif', 'waterfowl.tif', 'wellhead_protection.tif']

mlist = ['bird_watching.tif', 'lake_recreation.tif', 'pheasant.tif', 'population.tif',
         'sgcn_all.tif', 'upland_game_birds.tif', 'waterfowl.tif', 'wellhead_protection.tif']
# mlist = ['pollination.tif', 'sgcn_forest.tif', 'sgcn_grassland.tif', 'sgcn_wetland.tif', 'species_richness.tif',
#          'trout_angling.tif']

mlist = ['idx/' + x for x in mlist]

parcels = gp.read_file('boundaries/matched_controls_NEW2.shp')
parcels['gis_acres'] = parcels['geometry'].area / 4046.86
# parcels = parcels[['pid', 'geometry']]
# parcels = parcels[['uniqueid', 'geometry']]

# parcels_sample = parcels.sample(frac=0.1)
# parcels_sample['gis_acres'] = parcels_sample['geometry'].area / 4046.86
# parcels_sample.to_file('boundaries/sample10pct_forty_undev.shp')

parcels_scored = mean_parcel_score(parcels, mlist)

# parcels_scored.drop('geometry', axis=1, inplace=True)
# parcels_scored.to_csv('ls_scored.csv', index=False)
parcels_scored.to_file('scored/matched_controls_NEW2.shp_scored.shp')
