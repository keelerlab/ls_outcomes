import pandas as pd
import geopandas as gp

# Parcel data prep step 3

# This script filters all of the parcels in MN based on land cover and public private status calculated in the
# lulc_stats script. The output is a .csv that can be joined to a parcel shapefile to create a subset for 2_analysis.

parcel_table_list = ['forty']
# options: 'forty', 'forty_sample_1pct'

natveg_min_threshold = 0.5
water_max_threshold = 0.90
public_min_threshold = 0.5
developed_max_threshold = 0.3
yr = '16'

uspatial_13_map = {1: 'low_urban', 2: 'med_urban', 3: 'high_urban', 101: 'emergent_wetlands',
                   102: 'forested_wetlands',
                   103: 'water', 104: 'mining', 105: 'conifer_forest', 106: 'deciduous_forest', 107: 'mixed_forest',
                   108: 'managed+natural_grass', 109: 'hay_pasture', 110: 'row_crops'}
NLCD_map = {11: 'water', 21: 'developed_open', 22: 'developed_low', 23: 'developed_med', 24: 'developed_high',
            31: 'barren', 41: 'deciduous_forest', 42: 'evergreen_forest', 43: 'mixed_forest', 52: 'shrub_scrub',
            71: 'herbaceous', 81: 'hay_pasture', 82: 'cultivated_crops', 90: 'woody_wetlands',
            95: 'herb_wetlands'}

uspatial_13_all = ['1_uspatial_13', '2_uspatial_13', '3_uspatial_13', '101_uspatial_13', '102_uspatial_13',
                   '103_uspatial_13', '104_uspatial_13', '105_uspatial_13', '106_uspatial_13', '107_uspatial_13',
                   '108_uspatial_13', '109_uspatial_13', '110_uspatial_13']
uspatial_13_dev = ['2_uspatial_13', '3_uspatial_13']
uspatial_13_natveg = ['101_uspatial_13', '102_uspatial_13', '103_uspatial_13', '105_uspatial_13', '106_uspatial_13',
                      '107_uspatial_13', '108_uspatial_13']
uspatial_13_water = '103_uspatial_13'

NLCD_all = ['11_NLCD', '21_NLCD', '22_NLCD', '23_NLCD', '24_NLCD', '31_NLCD', '41_NLCD', '42_NLCD', '43_NLCD',
            '52_NLCD', '71_NLCD', '81_NLCD', '82_NLCD', '90_NLCD', '95_NLCD']
NLCD_natveg = ['11_NLCD', '41_NLCD', '42_NLCD', '43_NLCD', '52_NLCD', '71_NLCD', '90_NLCD', '95_NLCD']
NLCD_dev = ['22_NLCD', '23_NLCD', '24_NLCD']
NLCD_water = '11_NLCD'

NLCD_all = [x + '_' + yr for x in NLCD_all]
NLCD_natveg = [x + '_' + yr for x in NLCD_natveg]
NLCD_dev = [x + '_' + yr for x in NLCD_dev]
NLCD_water = NLCD_water + yr

for parcel_table in parcel_table_list:
    df = pd.read_csv('aux_data/' + parcel_table + '_aux.csv')
    # df.loc[:, uspatial_13_all] = df.loc[:, uspatial_13_all].fillna(0)
    # df['uspatial_13_cell_count'] = df[uspatial_13_all].sum(axis=1, numeric_only=True)
    # df['uspatial_13_natveg%'] = df[uspatial_13_natveg].sum(axis=1, numeric_only=True) / df['uspatial_13_cell_count']
    # df['uspatial_13_dev%'] = df[uspatial_13_dev].sum(axis=1, numeric_only=True) / df['uspatial_13_cell_count']
    # df['uspatial_13_water%'] = df[uspatial_13_water] / df['uspatial_13_cell_count']

    df.loc[:, NLCD_all] = df.loc[:, NLCD_all].fillna(0)
    df['NLCD_cell_count'] = df[NLCD_all].sum(axis=1, numeric_only=True)
    df['NLCD_dev%'] = df[NLCD_dev].sum(axis=1, numeric_only=True) / df['NLCD_cell_count']
    df['NLCD_water%'] = df[NLCD_water] / df['NLCD_cell_count']

    df = df.loc[df['NLCD_water%'] < water_max_threshold]
    df = df.loc[df['NLCD_dev%'] < developed_max_threshold]
    # df = df.loc[df['uspatial_13_dev%'] < developed_max_threshold]
    # df = df.loc[df['uspatial_13_water%'] < water_max_threshold]
    # df = df.loc[df['uspatial_13_natveg%'] > natveg_min_threshold]
    df = df.loc[df['mean_public_land'] < public_min_threshold]

    df = df[['forty_id', 'gis_acres']]
    print(df)

    all = gp.read_file(r'boundaries/pls_forty.shp')
    out = pd.merge(all, df, how='inner', on='forty_id')
    out.to_file(r'boundaries/forty_undev.shp')
