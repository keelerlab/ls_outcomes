import geopandas as gp
import pandas as pd
from rasterstats import zonal_stats

# Parcel data prep step 2

# This script should be run on the shapefile(s) that represent the 2_analysis units. It calculates the breakdown of all
# land covers in the unit for any number of land cover maps. It will also calculate other variables of interest, such
# as public private. The output is a table that can be joined back to a shapefile to identify parcels that meet land
# use requirements.


def mean_parcel_score(shapefile_df, aux_dict, stats):
    for aux_data in aux_dict:
        if stats[aux_data] == 'categorical':
            zonal_stats_df = zonal_stats(shapefile_df, aux_dict[aux_data], categorical=True)
            zonal_stats_df = pd.DataFrame(zonal_stats_df)
            zonal_stats_df = zonal_stats_df.add_suffix('_' + aux_data)
            shapefile_df = shapefile_df.join(zonal_stats_df)

        else:
            zonal_stats_df = zonal_stats(shapefile_df, aux_dict[aux_data], stats=stats[aux_data])
            zonal_stats_df = pd.DataFrame(zonal_stats_df)
            shapefile_df = shapefile_df.join(zonal_stats_df)
            for stat in stats[aux_data]:
                shapefile_df.rename(columns={stat: stat + '_' + aux_data}, inplace=True)

    return shapefile_df


aux_data_paths = {
    'NLCD_16': 'aux_data/nlcd_16.tif'}

stats_lookup = {
    'NLCD_16': 'categorical'}

shapefile_paths = {'tax_parcels': 'boundaries/Taxparcels_private_over1ac_NEW.shp'}
# shapefile_paths = {'tax_parcels': 'boundaries/wma.shp'}
# 'forty_sample_1pct': 'boundaries/pls_forty_sample_1pct.shp'

for shp in shapefile_paths:
    print(shp)
    shapefile = gp.read_file(shapefile_paths[shp])
    shapefile = mean_parcel_score(shapefile, aux_data_paths, stats_lookup)
    # shapefile['acres'] = shapefile['geometry'].area/4046.86
    shapefile.drop('geometry', axis=1, inplace=True)
    shapefile.to_csv('aux_data/'+shp+'_aux.csv', index=False)