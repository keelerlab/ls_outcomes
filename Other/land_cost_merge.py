import geopandas as gp
import pandas as pd
import pygeoprocessing as pgp
import gdal

# counties = ['MN_Aitkin_PRISM2018.shp']
counties = ['MN_Aitkin_PRISM2018.shp', 'MN_Anoka_PRISM2018.shp', 'MN_Becker_PRISM2018.shp', 'MN_Beltrami_PRISM2018.shp',
            'MN_Benton_PRISM2018.shp', 'MN_Big_Stone_PRISM2018.shp', 'MN_Blue_Earth_PRISM2018.shp',
            'MN_Brown_PRISM2018.shp', 'MN_Carlton_PRISM2018.shp', 'MN_Carver_PRISM2018.shp', 'MN_Cass_PRISM2018.shp',
            'MN_Chippewa_PRISM2018.shp', 'MN_Chisago_PRISM2018.shp', 'MN_Clay_PRISM2018.shp',
            'MN_Clearwater_PRISM2018.shp', 'MN_Cook_PRISM2018.shp', 'MN_Cottonwood_PRISM2018.shp',
            'MN_Crow_Wing_PRISM2018.shp', 'MN_Dakota_PRISM2018.shp', 'MN_Dodge_PRISM2018.shp',
            'MN_Douglas_PRISM2018.shp', 'MN_Faribault_PRISM2018.shp', 'MN_Fillmore_PRISM2018.shp',
            'MN_Freeborn_PRISM2018.shp', 'MN_Goodhue_PRISM2018.shp', 'MN_Grant_PRISM2018.shp',
            'MN_Hennepin_1_PRISM2018.shp', 'MN_Hennepin_2_PRISM2018.shp', 'MN_Houston_PRISM2018.shp',
            'MN_Hubbard_PRISM2018.shp', 'MN_Isanti_PRISM2018.shp', 'MN_Itasca_PRISM2018.shp',
            'MN_Jackson_PRISM2018.shp', 'MN_Kanabec_PRISM2018.shp', 'MN_Kandiyohi_PRISM2018.shp',
            'MN_Kittson_PRISM2018.shp', 'MN_Koochiching_PRISM2018.shp', 'MN_Lac_qui_Parle_PRISM2018.shp',
            'MN_Lake_of_the_Woods_PRISM2018.shp', 'MN_Lake_PRISM2018.shp', 'MN_Le_Sueur_PRISM2018.shp',
            'MN_Lincoln_PRISM2018.shp', 'MN_Lyon_PRISM2018.shp', 'MN_Mahnomen_PRISM2018.shp',
            'MN_Marshall_PRISM2018.shp', 'MN_Martin_PRISM2018.shp', 'MN_McLeod_PRISM2018.shp',
            'MN_Meeker_PRISM2018.shp', 'MN_Mille_Lacs_PRISM2018.shp', 'MN_Morrison_PRISM2018.shp',
            'MN_Mower_PRISM2018.shp', 'MN_Murray_PRISM2018.shp', 'MN_Nicollet_PRISM2018.shp', 'MN_Nobles_PRISM2018.shp',
            'MN_Norman_PRISM2018.shp', 'MN_Olmsted_PRISM2018.shp', 'MN_Otter_Tail_PRISM2018.shp',
            'MN_Pennington_PRISM2018.shp', 'MN_Pine_PRISM2018.shp', 'MN_Pipestone_PRISM2018.shp',
            'MN_Polk_PRISM2018.shp', 'MN_Pope_PRISM2018.shp', 'MN_Ramsey_PRISM2018.shp', 'MN_Red_Lake_PRISM2018.shp',
            'MN_Redwood_PRISM2018.shp', 'MN_Renville_PRISM2018.shp', 'MN_Rice_PRISM2018.shp', 'MN_Rock_PRISM2018.shp',
            'MN_Roseau_PRISM2018.shp', 'MN_Scott_PRISM2018.shp', 'MN_Sherburne_PRISM2018.shp',
            'MN_Sibley_PRISM2018.shp', 'MN_St_Louis_PRISM2018.shp', 'MN_Stearns_PRISM2018.shp',
            'MN_Steele_PRISM2018.shp', 'MN_Stevens_PRISM2018.shp', 'MN_Swift_PRISM2018.shp', 'MN_Todd_PRISM2018.shp',
            'MN_Traverse_PRISM2018.shp', 'MN_Wabasha_PRISM2018.shp', 'MN_Wadena_PRISM2018.shp',
            'MN_Waseca_PRISM2018.shp', 'MN_Washington_PRISM2018.shp', 'MN_Watonwan_PRISM2018.shp',
            'MN_Wilkin_PRISM2018.shp', 'MN_Winona_PRISM2018.shp', 'MN_Wright_PRISM2018.shp',
            'MN_Yellow_Medicine_PRISM2018.shp']

vars = ['MKT_VAL_LA', 'landemv_am', 'geometry']
state = gp.GeoDataFrame()
counter = 0
shps = 'c:/users/rrnoe/desktop/shp_gt1ac/'
mkts = 'c:/users/rrnoe/desktop/mkt_gt1ac/'
emvs = 'c:/users/rrnoe/desktop/emv_gt1ac/'

for county in counties:
    print(counter)
    counter += 1
    gdf = gp.read_file(county)
    gdf = gdf[vars]
    gdf = gdf.to_crs('epsg:26915')
    gdf['acres'] = gdf['geometry'].area / 4047
    gdf = gdf.loc[gdf['acres'] > 1]
    gdf['MKT_acre'] = gdf['MKT_VAL_LA'] / gdf['acres']
    gdf['emv_acre'] = gdf['landemv_am'] / gdf['acres']
    # gdf = gdf[['MKT_acre', 'emv_acre', 'geometry']]
    # state = pd.concat([state, gdf])
    gdf.to_file(shps + county)

    pgp.create_raster_from_vector_extents(shps + county, mkts + county+'.tif', [30, -30], gdal.GDT_Float32, -999, fill_value=0)
    pgp.create_raster_from_vector_extents(shps + county, emvs + county+'.tif', [30, -30], gdal.GDT_Float32, -999, fill_value=0)
    pgp.rasterize(shps + county, mkts + county+'.tif', burn_values=[0], option_list=["ATTRIBUTE=MKT_acre", "COMPRESS=LZW"])
    pgp.rasterize(shps + county, emvs + county+'.tif', burn_values=[0], option_list=["ATTRIBUTE=emv_acre", "COMPRESS=LZW"])


