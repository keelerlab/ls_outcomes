import xarray as xr
import pandas as pd
import numpy as np
import rioxarray

# loop to open all metrics, merge them and convert them to dataframe
mdb = pd.read_csv('auxdata/mtable.csv', index_col='metric')
mdb = mdb.loc[mdb['candidate'] == 'yes']
mlist = mdb.index.tolist()

nlcd = xr.open_rasterio('aligned/nlcd_16.tif').squeeze().drop("band")
nlcd.name = 'nlcd_16'
df = nlcd.to_dataframe('nlcd_16')
df.astype('int8', copy=False)
del nlcd
counter = 1

for metric in mlist:
    print(metric)
    metric_array = xr.open_rasterio('idx/'+metric+'.tif').squeeze().drop("band")
    metric_array.name = metric
    metric_df = metric_array.to_dataframe(metric)
    del metric_array
    df = pd.concat([df, metric_df], axis=1)
    del metric_df
    df[metric].replace(to_replace=0, value=np.nan, inplace=True)
    df.loc[df['nlcd_16'].isin([0, 22, 23, 24, 128]), metric] = np.nan  # or [11, 22, 23, 24, 128]
    if mdb.loc[metric, 'categorical'] == 'yes':
        best_list = mdb.loc[metric, 'best_list']
        best_list = eval(best_list)
        df.loc[df[metric].isin(best_list), 'q'+metric] = 1

    else:
        df['q' + metric] = pd.qcut(df[metric], 4, labels=False)
        df.loc[df['q' + metric] < 1.9, 'q' + metric] = 0  # can implement the method above if more flexibility is desired
        df.loc[df['q' + metric] > 1.9, 'q' + metric] = 1
        

    df.drop(columns=[metric], inplace=True)
    if counter == 1:
        df['mb_sum'] = 0
        df.loc[df['nlcd_16'].isin([0, 22, 23, 24, 128]), 'mb_sum'] = np.nan
        df['mb_sum'] = df[['mb_sum', 'q'+metric]].sum(axis=1, skipna=True)
        counter += 1
    else:
        df['mb_sum'] = df[['mb_sum', 'q'+metric]].sum(axis=1, skipna=True)
    # out_dataset = pd.DataFrame.to_xarray(df['q' + metric])
    # out_dataset.rio.write_crs('epsg:26915', inplace=True)
    # out_dataset.rio.to_raster('bin_'+metric+'.tif', compress='lzw')
    # del out_dataset
    df.drop(columns=['q' + metric], inplace=True)


con = xr.open_rasterio('aligned/conservation.tif').squeeze().drop("band")
con.name = 'conservation'
con_df = con.to_dataframe('conservation')
# con_df['conservation'].replace(to_replace=0, value=2, inplace=True)
del con
df = pd.concat([df, con_df], axis=1)
del con_df
df.loc[df['nlcd_16'].isin([0, 22, 23, 24, 128]), 'conservation'] = np.nan
df.drop(columns=['nlcd_16'], inplace=True)

crosstab = pd.crosstab(df['mb_sum'], df['conservation'])
crosstab.index.rename('metric', inplace=True)
crosstab.reset_index(inplace=True)
crosstab.to_csv('multiple_benefit_tophalf.csv', index=False)
del crosstab
df.drop(columns=['conservation'], inplace=True)

# out_dataset = pd.DataFrame.to_xarray(df['mb_sum'])
# del df
# out_dataset.rio.write_crs('epsg:26915', inplace=True)
# out_dataset.rio.to_raster('multiple_benefit_sum_90.tif')
