import xarray as xr
import pandas as pd
import numpy as np

# mask the metric arrays to remove developed, water, (maybe ag but probably not) and if applicable, metric 0s
# calculate quantiles with qcut and reclassify dataframe
# calculate the area of each program (including private undeveloped) in each bin (crosstab)

mdb = pd.read_csv('auxdata/mtable.csv', index_col='metric')
mdb = mdb.loc[mdb['candidate'] == 'yes']
mlist = mdb.index.tolist()

# loop to open all metrics, merge them and convert them to dataframe
df = pd.DataFrame()
ct = pd.DataFrame()

con = xr.open_rasterio('aligned/conservation.tif').squeeze().drop("band")
con.name = 'conservation'
con_df = con.to_dataframe('conservation')


nlcd = xr.open_rasterio('aligned/nlcd_16.tif').squeeze().drop("band")
nlcd.name = 'nlcd_16'
nlcd_df = nlcd.to_dataframe('nlcd_16')

df = pd.concat([con_df, nlcd_df], axis=1)
del nlcd
del nlcd_df
del con
del con_df
# df['conservation'].replace(to_replace=0, value=2, inplace=True)

for metric in mlist:
    print(metric)
    metric_array = xr.open_rasterio('idx/'+metric+'.tif').squeeze().drop("band")
    metric_array.name = metric
    metric_df = metric_array.to_dataframe(metric)
    del metric_array
    df = pd.concat([df, metric_df], axis=1)
    del metric_df
    df.replace(to_replace=0, value=np.nan, inplace=True)
    df.loc[df['nlcd_16'].isin([0, 22, 23, 24, 128]), metric] = np.nan  # or [11, 22, 23, 24, 128]

    if mdb.loc[metric, 'categorical'] == 'yes':
        categories = eval(mdb.loc[metric, 'categories'])
        for category in categories:
            df.loc[df[metric] == int(category), 'q'+metric] = categories[category] + '-' + metric

    else:
        df['q'+metric] = pd.qcut(df[metric], 4, labels=('Low-' + metric, 'Moderate-' + metric, 'High-' + metric,
                                                          'Best-' + metric))
    crosstab = pd.crosstab(df['q' + metric], df['conservation'])
    crosstab.index.rename('metric', inplace=True)
    crosstab.reset_index(inplace=True)
    ct = pd.concat([ct, crosstab], axis=0)
    df.drop(columns=[metric, 'q'+metric], inplace=True)

ct[['quality', 'name']] = ct['metric'].str.split('-',  expand=True,)
ct.to_csv('crosstab.csv', index=False)









