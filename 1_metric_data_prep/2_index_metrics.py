from osgeo.gdalnumeric import *
import numpy as np
import gdal
import pandas as pd

# Step 2. Given a folder of aligned maps, index each so that each starts at 0, and ends at 1.


########################################################################################################################
# Functions
def raster_to_array(gdal_raster, out_ndv):
    ds = gdal.Open(gdal_raster)
    band = ds.GetRasterBand(1)
    in_ndv = band.GetNoDataValue()
    raster_array = BandReadAsArray(band)
    raster_array[raster_array == in_ndv] = out_ndv
    return raster_array


def make_array_index(in_array, mask_value):
    mask = in_array == mask_value
    masked_array = np.ma.masked_array(in_array, mask)
    array_min = np.nanmin(masked_array)
    array_max = np.nanmax(masked_array)
    array_range = array_max - array_min
    idx_array = (masked_array - array_min) / array_range
    return idx_array


def array_to_gtiff(in_arrary, template, out_name, no_data_value):
    x_size = template.RasterXSize
    y_size = template.RasterYSize
    out_raster = gdal.GetDriverByName('GTiff').Create(out_name, x_size, y_size, 1, gdal.GDT_Float32,
                                                      options=['COMPRESS=LZW'])
    out_raster.SetGeoTransform(template.GetGeoTransform())
    out_raster.SetProjection(template.GetProjection())
    band = out_raster.GetRasterBand(1)
    band.SetNoDataValue(no_data_value)
    BandWriteArray(band, in_arrary)
    out_raster = None
    band = None


########################################################################################################################

template_file = gdal.Open('aligned/nlcd_16.tif')
ndv = -999
mdb = pd.read_csv('auxdata/mtable.csv', index_col='metric')
mdb = mdb.loc[mdb['candidate'] == 'yes']
mlist = mdb.index.tolist()

for raster in mlist:
    raster_path = 'aligned/' + raster + '.tif'
    arr = raster_to_array(raster_path, ndv)
    if mdb.loc[raster, 'categorical'] == 'no':
        arr_idx = make_array_index(arr, ndv)
        array_to_gtiff(arr_idx, template_file, 'idx/' + raster + '.tif', ndv)
    else:
        array_to_gtiff(arr, template_file, 'idx/' + raster + '.tif', ndv)
