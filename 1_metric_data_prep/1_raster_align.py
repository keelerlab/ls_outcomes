import pygeoprocessing as pg
import pandas as pd

# Step 1. Once all metrics are created and in the same projection, use this script to align and resize them so grids
# align perfectly. Change 'union' to 'intersection' if all map extents are >= the state.

res = 300
metric_dir = 'c:/users/rrnoe/work/metrics/'
mdb_path = 'auxdata/mtable.csv'
auxlist = ['conservation.tif', 'nlcd_16.tif']
auxlist_rs = ['near', 'near']

mdb = pd.read_csv(mdb_path)
mdb = mdb.loc[mdb['candidate'] == 'yes']
mlist = mdb['metric'].tolist()
mlist_rs = mdb['resample'].tolist()
aligned = ['aligned/' + x + '.tif' for x in mlist] + ['aligned/' + x for x in auxlist]
mlist = [metric_dir + x + '/' + x + '.tif' for x in mlist] + ['auxdata/30m/' + x for x in auxlist]
mlist_rs = mlist_rs + auxlist_rs

pg.align_and_resize_raster_stack(mlist, aligned, mlist_rs, [res, -res], 'intersection', raster_align_index=len(mlist)-1)
