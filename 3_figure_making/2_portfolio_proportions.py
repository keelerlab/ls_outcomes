import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams.update({'font.size': 12})

# loop to open all metrics, merge them and convert them to dataframe
mdb = pd.read_csv('auxdata/mtable.csv', index_col='metric')
mdb = mdb.loc[mdb['candidate'] == 'yes']
mlist = mdb.index.tolist()

df = pd.read_csv('crosstab.csv')
df.set_index('metric', inplace=True)

df.rename(columns={'1': 'other', '4': 'wma', '9': 'ls', '15': 'unprotected'}, inplace=True)

df = df[df['name'].isin(mlist)]
df['total_all'] = df['other'] + df['wma'] + df['ls'] + df['unprotected']
grouped = df.groupby(by='name').sum()
state_area = grouped['total_all'].max()
ls_area = grouped['ls'].max()

b_list = ['Best']
for metric in mlist:
    if mdb.loc[metric, 'categorical'] == 'yes':
        categories = eval(mdb.loc[metric, 'categories'])
        best_categories = eval(mdb.loc[metric, 'best_list'])
        for item in best_categories:
            b_list.append(categories[item])

df = df.loc[df['quality'].isin(b_list)]
df = df.groupby(by='name').sum().reset_index()
df['total'] = df['other'] + df['wma'] + df['ls']
df['best_resource_area'] = df['other'] + df['wma'] + df['ls'] + df['unprotected']
df['best_resource_pct'] = df['best_resource_area'] / state_area
df['ls_best_pct'] = df['ls'] / ls_area
# df['result'] = df['ls_best_pct'] / df['best_resource_pct']
df['result'] = (df['ls_best_pct'] - df['best_resource_pct']) / df['best_resource_pct']

df.sort_values('result', ascending=False, inplace=True)
result = df['result'].tolist()
result_names = df['name'].tolist()
print_names = [mdb.loc[x, 'print_name'] for x in result_names]

width = 0.3  # the width of the bars: can also be len(x) sequence
ind = np.arange(len(mlist))
fig = plt.figure(figsize=(14, 10))
ax = fig.add_subplot(1, 1, 1)
p2 = plt.bar(ind, result, width, color='#FF7F0E')

plt.ylabel('Proportion of OHF Portfolio')
plt.title('Proportion of OHF portfolio in Highest Quality Category\n '
          'Relative to Amount of Highest Quality Land in the State')
vals = ax.get_yticks()
ax.set_yticklabels(['{:,.0%}'.format(x) for x in vals])
plt.axhline(y=0, color='g', linestyle='-')
plt.xticks(ind, print_names, rotation=90)
fig.savefig('portfolio.png', bbox_inches='tight')
plt.show()
