import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams.update({'font.size': 14})

mdb = pd.read_csv('auxdata/mtable.csv', index_col='metric')
mdb = mdb.loc[mdb['candidate'] == 'yes']
mlist = mdb.index.tolist()

df = pd.read_csv('crosstab.csv')
df.set_index('metric', inplace=True)
df.rename(columns={'1': 'other', '4': 'wma', '9': 'ls', '15': 'unprotected'}, inplace=True)
df = df[['other', 'wma', 'ls', 'unprotected']] * 0.2223869

for metric in mlist:
    ls_list = []
    wma_list = []
    other_list = []

    if mdb.loc[metric, 'categorical'] == 'yes':
        qb_list = eval(mdb.loc[metric, 'qb_list'])
        categories = eval(mdb.loc[metric, 'categories'])
        quality_levels = []
        for val in qb_list:
            quality_levels.append(categories[val])

    else:
        quality_levels = ['Moderate', 'High', 'Best']

    for quality in quality_levels:
        ls_val = df.loc[quality + '-' + metric, 'ls']
        ls_list.append(ls_val)

        wma_val = df.loc[quality + '-' + metric, 'wma']
        wma_list.append(wma_val)

        other_val = df.loc[quality + '-' + metric, 'other']
        other_list.append(other_val)

    width = 0.3
    ind = np.arange(len(ls_list))
    bars_1and2 = np.add(ls_list, wma_list).tolist()
    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(1, 1, 1)
    if len(ls_list) == 1:
        ax.set_xlim(-0.5, 1.5)
    p2 = plt.bar(ind, ls_list, width, color='#FF7F0E', bottom=wma_list)
    p1 = plt.bar(ind, wma_list, width, color='#1F77B4')
    # p0 = plt.bar(ind, other_list, width, color='green', bottom=bars_1and2)
    plt.ylabel('Number of Acres')
    plt.title(mdb.loc[metric, 'print_name'] + '\nQuality of Outdoor Heritage Fund Acquisitions')
    if mdb.loc[metric, 'categorical'] == 'yes':
        plt.xticks(ind, quality_levels)
    else:
        plt.xticks(ind, ('Moderate\n25-50th %tile', 'High\n50-75th %tile', 'Very High\n >75th %tile'))
    ax.set_yticklabels(['{:,}'.format(int(x)) for x in ax.get_yticks().tolist()])
    # plt.legend((p0[0], p2[0], p1[0]), ('All other conservation', 'Lessard-Sams\n acquisitions', 'WMAs without \nL-S acquisitions'))
    plt.legend((p2[0], p1[0]), ('Outdoor Heritage Fund\n acquisitions', 'WMAs without \nOHF acquisitions'))
    # ax.legend(('Lessard-Sams\nacquisitions',))
    fig.savefig(metric+'_ls.png', bbox_inches='tight')
    plt.show()
